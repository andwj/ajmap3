#
#  Common makefile rules for AJMAP3.
#

DUMMY_FILE=$(BUILD_DIR)/zzdummy

#
# Object files
#

OBJS=\
	$(BUILD_DIR)/src/brush.o \
	$(BUILD_DIR)/src/brush_primit.o \
	$(BUILD_DIR)/src/bsp.o \
	$(BUILD_DIR)/src/bspfile.o \
	$(BUILD_DIR)/src/bspfile_ibsp.o \
	$(BUILD_DIR)/src/bspfile_rbsp.o \
	$(BUILD_DIR)/src/config.o \
	$(BUILD_DIR)/src/decals.o \
	$(BUILD_DIR)/src/extra_tools.o \
	$(BUILD_DIR)/src/facebsp.o \
	$(BUILD_DIR)/src/fog.o \
	$(BUILD_DIR)/src/image.o \
	$(BUILD_DIR)/src/leakfile.o \
	$(BUILD_DIR)/src/light_bounce.o \
	$(BUILD_DIR)/src/light.o \
	$(BUILD_DIR)/src/lightmaps.o \
	$(BUILD_DIR)/src/lightmaps_ydnar.o \
	$(BUILD_DIR)/src/light_trace.o \
	$(BUILD_DIR)/src/light_ydnar.o \
	$(BUILD_DIR)/src/main.o \
	$(BUILD_DIR)/src/map.o \
	$(BUILD_DIR)/src/mesh.o \
	$(BUILD_DIR)/src/model.o \
	$(BUILD_DIR)/src/patch.o \
	$(BUILD_DIR)/src/portals.o \
	$(BUILD_DIR)/src/prtfile.o \
	$(BUILD_DIR)/src/shaders.o \
	$(BUILD_DIR)/src/surface.o \
	$(BUILD_DIR)/src/surface_extra.o \
	$(BUILD_DIR)/src/surface_foliage.o \
	$(BUILD_DIR)/src/surface_fur.o \
	$(BUILD_DIR)/src/surface_meta.o \
	$(BUILD_DIR)/src/tjunction.o \
	$(BUILD_DIR)/src/tree.o \
	$(BUILD_DIR)/src/vis.o \
	$(BUILD_DIR)/src/visflow.o \
	$(BUILD_DIR)/src/writebsp.o \
	\
	$(BUILD_DIR)/common/cmdlib.o \
	$(BUILD_DIR)/common/ddslib.o \
	$(BUILD_DIR)/common/dye.o \
	$(BUILD_DIR)/common/filematch.o \
	$(BUILD_DIR)/common/imagelib.o \
	$(BUILD_DIR)/common/inout.o \
	$(BUILD_DIR)/common/jpeg.o \
	$(BUILD_DIR)/common/md4.o \
	$(BUILD_DIR)/common/polylib.o \
	$(BUILD_DIR)/common/qthreads.o \
	$(BUILD_DIR)/common/scriplib.o \
	$(BUILD_DIR)/common/unzip.o \
	$(BUILD_DIR)/common/vfs.o \
	\
	$(BUILD_DIR)/mathlib/m4x4.o \
	$(BUILD_DIR)/mathlib/mathlib.o \
	\
	$(BUILD_DIR)/picomodel/picointernal.o \
	$(BUILD_DIR)/picomodel/picomodel.o \
	$(BUILD_DIR)/picomodel/picomodules.o \
	$(BUILD_DIR)/picomodel/pm_3ds.o \
	$(BUILD_DIR)/picomodel/pm_ase.o \
	$(BUILD_DIR)/picomodel/pm_fm.o \
	$(BUILD_DIR)/picomodel/pm_md2.o \
	$(BUILD_DIR)/picomodel/pm_md3.o \
	$(BUILD_DIR)/picomodel/pm_mdc.o \
	$(BUILD_DIR)/picomodel/pm_ms3d.o \
	$(BUILD_DIR)/picomodel/pm_obj.o \
	$(BUILD_DIR)/picomodel/pm_terrain.o

$(BUILD_DIR)/src/%.o: src/%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

$(BUILD_DIR)/common/%.o: common/%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

$(BUILD_DIR)/mathlib/%.o: mathlib/%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

$(BUILD_DIR)/picomodel/%.o: picomodel/%.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

#
# Targets
#

all: $(DUMMY_FILE) $(PROGRAM)

clean:
	rm -f $(PROGRAM) $(BUILD_DIR)/*/*.[oa]
	rm -f core core.* ERRS

# this is used to create the BUILD_DIR directory
$(DUMMY_FILE):
	mkdir -p $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)/src
	mkdir -p $(BUILD_DIR)/common
	mkdir -p $(BUILD_DIR)/mathlib
	mkdir -p $(BUILD_DIR)/picomodel
	@touch $@

$(PROGRAM): $(OBJS)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS)

stripped: all
	$(STRIP) $(STRIP_FLAGS) $(PROGRAM)

.PHONY: all clean stripped

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
