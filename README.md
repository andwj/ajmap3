
AJMAP3
======

by Andrew Apted, 2020.


About
-----

AJMAP3 is a command-line tool for compiling Quake3 map files
(among other things).  It is a fork of the "q3map2" program,
but with some very significant changes so I have given this
a distinct name to avoid confusion.


Differences to Q3MAP2
---------------------

* codebase is stand-alone, not part of the editor source
* uses CFG files associated with a map
* simpler command-line options, no path assumptions
* better terminal output with colored text
* better logging, can specify log file or disable it altogether
* only supports Quake 3 (currently)
* no editor integration via XML messages


Website
-------

https://gitlab.com/andwj/ajmap3


Binary Packages
---------------

https://gitlab.com/andwj/ajmap3/tags/v3.1.6

Only Windows has binary packages, for Linux (etc) you will need to
clone this repository and compile the source code.


Legalese
--------

AJMAP3 is Free Software, under the terms of the GNU General Public
License, version 2 or (at your option) any later version.
See the [LICENSE.txt](LICENSE.txt) file for the complete text.

AJMAP3 comes with NO WARRANTY of any kind, express or implied.
Please read the license for full details.


Acknowledgements
----------------

Id Software : created Quake III Arena and released the code for
the engine and various tools under a free software license.

Randy "ydnar" Reddig : took the original Q3MAP tool and created
Q3MAP2 which fixed many bugs and added numerous enhancements.

The GTKRadiant developers : made a cross-platform version of
Id Software's Q3Radiant editor using the GTK+ toolkit.
See: https://icculus.org/gtkradiant

The NetRadiant developers : made a fork of GTKRadiant 1.5 in
order to preserve its user interface and workflow.
See: https://gitlab.com/xonotic/netradiant

Garux (github user) : created a custom version of NetRadiant.
AJMAP3 began as a fork of q3map2 from this codebase.
See: https://github.com/garux/netradiant-custom


Compiling
---------

Firstly, ensure you have all the required libraries
(see the next section for more information).

On Linux, just type "make".

On BSD systems, just type "gmake" (GNU make is required).

The Windows binary is built on Linux using a cross compiler,
and there is currently no support for building natively.


Required Libraries
------------------

* libjpeg
* libpng
* zlib


Usage
-----

Normal usage looks like this:
```
    ajmap3 <command> FILE [OPTIONS....]
```

Running AJMAP3 without any arguments, or using the `-h` or
`--help` options, will show a brief summary of the available
command-line options.  Options begin with either a single or
double dash (`-`) character, and can appear on the command-line
in any order.  Only a single file can be processed at a time.

Note that if you want to supply settings for lighting or bsp
or vis generation, you need to use a map-specific config file
(see the section about config files below).  AJMAP3 does not
accept such settings (like `-fast`) on the command-line.


Available Commands
------------------

The following commands are available:

`-help  TOPIC`  
Display some help text about a particular command or topic.
For commands like `-bsp` and `-light`, this will show a list
of every possible setting which can be used in a CFG file,
with a short description of each one.  When the topic is
`options` (or left out), a brief summary of options is shown.
The topic may also be `commands` to show a summary of each
command, or `games` to show a list of supported games.

`-bsp  FILE.map`  
Compiles the `.map` file which your editor uses into a BSP
file which the game engine can load.

`-light  FILE.bsp`  
Processes lighting for the given BSP file.

`-vis  FILE.bsp`  
Calculates visibility for the given BSP file.

`-import  FILE.bsp`  
Copies lightmaps from `file/lightmap_####.tga` files into the BSP,
where #### is a numeric id starting at zero.  Image files which
are absent are skipped with a warning.  Note that in Quake3, each
lightmap is 128x128 luxels and can contain multiple surfaces.

`-export  FILE.bsp`  
Copies lightmaps from the BSP to `file/lightmap_####.tga` files,
where #### is a numeric id starting at zero.  Note that in Quake3,
each lightmap is 128x128 luxels and can contain multiple surfaces.

`-exportents  FILE.bsp`  
Exports the entities in the BSP to a text file.  The output
filename is the same as the input filename but with the `.bsp`
extension changed to `.ent`.

`-fixaas  FILE.bsp`  
Fixes the checksum in the corresponding `.aas` file (used for bot
navigation) to match the given `.bsp` file, forcing the game engine
to accept it.  This is strongly discouraged, it is much safer to
simply rebuild the `.aas` file using BSPC or AJBOT3.

`-analyze  FILE.bsp`  
Analyzes all the lumps inside a BSP file, showing detailed information
about each one and uses heuristics to determine the possible contents.
The alternate spelling `-analyse` is also accepted.

`-info  FILE.bsp`  
Displays statistics about the contents of a BSP file.


Available Options
-----------------

`-game  GAME`  
Use settings appropriate for the given game, which must be one
supported by AJMAP3.  The default is "quake3".

`-path  DIRECTORY`  
Specify a directory where the game's PK3 files are located.
Each PK3 file in that directory will be added to a virtual file
system from which AJMAP3 can find and load files (like what the
game engine itself does).  Plain files in the specified path
can also be found and read, such as `scripts/shaderlist.txt`.

Only a single directory can follow this option, but this option
can be used more than once to look in multiple directories.

Note that the directory containing the input map file is
implicitly added to the list, allowing map-specific shaders
and textures to be found without doing anything special.

`-log  FILENAME`  
The default log filename is the same as the input filename
with the extension changed to `.log`, however this option lets
you specify the log filename directly.  The keyword `none` is
also accepted and will inhibit the creation of a log file.

`-cfg  FILENAME`  
The normal CFG filename is the same as the input filename
with the extension changed to `.cfg`, however this option lets
you specify a different filename.  You will get a fatal error
if this file cannot be found, unlike the normal behavior where
the implicitly determined config file is allowed to be absent.

`-threads  NUM`  
Number of CPU threads to use.  Using "1" will force AJMAP3
to run single-threaded (which tends to be much slower).
The default is "4" and is usually fine.

`-verbose`  
Enable more verbose output on the terminal.  Normally only
important messages are shown on the terminal, but this option
causes everything which is written to the log file to also be
displayed.  Note that `-v` is also accepted.

`-extra`  
Show messages containing extra detail about the current build
phase.  Many of these messages are about minor issues which
generally don't have any noticeable effect, but some of them
may indicate a serious problem in your map.

`-leaktest`  
When building a BSP, a leak is a fatal error.

`-force`  
Allow reading some BSP files which appear broken or invalid.
This is most likely to cause AJMAP3 to crash, so I recommend
never using this unless you are 100% sure it is needed.


Config Files
------------

Config files are a feature of the AJMAP3 and AJBOT3 tools.
Each `.map` file can have a config file associated with it,
typically it is the same filename with the `.cfg` extension
(though a different file can be specified via a command-line
option).  These files contain settings to control how each
part of the map compiling process operates.

The syntax of this file is quite simple, it consists of one
or more sections which is introduced by a keyword and is
followed by a `{` opening brace, then a group of settings,
and is terminated by a `}` closing brace.  Each setting is
a keyword followed by a value, however for boolean settings
the value can be omitted ("1" will be assumed, since most
boolean settings have a default of "0" i.e. disabled).
Comments begin with `//` and continue to the end of the
line, and are completely ignored.

An example CFG file:
```
    bsp
    {
    }

    light
    {
       fast
       samples 2
       bounce 1
    }

    aas
    {
       grapple
    }
```

For AJMAP3 there are four sections which are read, all
other sections are ignored.  These sections are: `bsp` for
settings related to BSP generation, `light` for lighting
settings, `vis` for settings related to the visibility
phase (computing the PVS), and `shaders` which provides a
list of shader filenames to load.  For a list of available
settings, run AJMAP3 with the `-help` option followed by
the name of the section.

The `shaders` section is a new feature of AJMAP3.  It is
just like the "shaderlist.txt" file, providing a list of
plain shader filenames (lacking any file extension or any
directory components).  When this section is used and is
not empty, reading the "shaderlist.txt" file is inhibited
unless the special keyword `$shaderlist` also appears in
the list of filenames.

