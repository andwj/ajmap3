#
#  --- AJMAP3 ---
#
#  Makefile for Linux and BSD systems.
#  Requires GNU make.
#

PROGRAM=ajmap3

# CC=clang
# CC=tcc

STRIP=strip

WARNINGS=-Wall -Wextra -Wno-unused-parameter -Wno-format-truncation
OPTIMISE=-O3 -g0
STRIP_FLAGS=--strip-unneeded

# default flags for compiler, preprocessor and linker
CFLAGS ?= $(OPTIMISE) $(WARNINGS)
CPPFLAGS ?=
LDFLAGS ?= $(OPTIMISE)
LIBS ?=

# needed system libraries
LIBS += -lm -lpthread -lpng -ljpeg -lz

CPPFLAGS += -Icommon -Imathlib -Ipicomodel

LDFLAGS += -Wl,--warn-common

# uncomment this for a fully statically linked binary:
# LDFLAGS += -static

# I needed this when using -m32 and -static together:
# LDFLAGS += -L/usr/lib/gcc/i686-linux-gnu/6/

# set directory where object files are stored
BUILD_DIR=build

include include.mk

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
