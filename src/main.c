/* -------------------------------------------------------------------------------;

   Copyright (C) 1999-2007 id Software, Inc. and contributors.
   For a list of contributors, see the accompanying CONTRIBUTORS file.

   This file is part of GtkRadiant.

   GtkRadiant is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   GtkRadiant is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GtkRadiant; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

   -------------------------------------------------------------------------------

   This code has been altered significantly from its original form, to support
   several games based on the Quake III Arena engine, in the form of "Q3Map2."

   ------------------------------------------------------------------------------- */


/* marker */
#define MAIN_C

/* dependencies */
#include "main.h"

#ifdef _WIN32
#include <windows.h>
#endif


#define AJMAP_VERSION  "3.1.6"


const char *surfaceTypes[ NUM_SURFACE_TYPES ] =
{
	"SURFACE_BAD",
	"SURFACE_FACE",
	"SURFACE_PATCH",
	"SURFACE_TRIANGLES",
	"SURFACE_FLARE",
	"SURFACE_FOLIAGE",
	"SURFACE_FORCED_META",
	"SURFACE_META",
	"SURFACE_FOGHULL",
	"SURFACE_DECAL",
	"SURFACE_SHADER"
};


#define AJ_PARSE_ARG_IMPL
#include "aj_parse_arg.h"

static int MatchBool(const char *long_name, const char *short_name, qboolean *var)
{
	if (aj_arg_Match(long_name, short_name))
	{
		*var = qtrue;
		return 1;
	}

	return 0;
}


static const char *log_override = NULL;
static const char *cfg_override = NULL;

const char *DetermineLogFile( const char *source ) {
	if (log_override != NULL) {
		if (Q_stricmp(log_override, "none") == 0 ||
			Q_stricmp(log_override, "/dev/null") == 0) {

			return "";
		}

		return log_override;
	}

	return Sys_LogNameFromSource( source );
}

const char *DetermineCfgFile( const char *source ) {
	if (cfg_override != NULL) {
		if (Q_stricmp(cfg_override, "none") == 0 ||
			Q_stricmp(cfg_override, "/dev/null") == 0) {

			return "";
		}

		return cfg_override;
	}

	return Sys_CfgNameFromSource( source );
}


/*
   Random()
   returns a pseudorandom number between 0.0 and 1.0 (inclusive)
 */

vec_t Random( void ) {
	return (vec_t) rand() / (vec_t) RAND_MAX;
}


/*
   CleanPath() - ydnar
   cleans a dos path \ -> /
 */

static void CleanPath( char *path ) {
	while ( *path )
	{
		if ( *path == '\\' ) {
			*path = '/';
		}
		path++;
	}
}


/*
   GetGame() - ydnar
   gets the game_t based on a -game argument.
   returns NULL if no match found.
 */
static game_t *GetGame( const char *arg ) {
	int i;

	/* dummy check */
	if ( arg == NULL || arg[0] == '\0' ) {
		return NULL;
	}

	/* joke */
	if ( !Q_stricmp( arg, "quake1" ) ||
		 !Q_stricmp( arg, "quake2" ) ||
		 !Q_stricmp( arg, "unreal" ) ||
		 !Q_stricmp( arg, "ut2k3" ) ||
		 !Q_stricmp( arg, "doom" ) ||
		 !Q_stricmp( arg, "doom2" ) ||
		 !Q_stricmp( arg, "dn3d" ) ||
		 !Q_stricmp( arg, "dnf" ) ||
		 !Q_stricmp( arg, "hl" ) ) {
		Sys_Printf( "April fools, silly rabbit!\n" );
		exit( 0 );
	}

	/* test it */
	i = 0;
	while ( games[i].arg != NULL )
	{
		if ( Q_stricmp( arg, games[i].arg ) == 0 ) {
			return &games[i];
		}
		i++;
	}

	/* no matching game */
	return NULL;
}


static void AddPath( const char *path ) {
	char *clean;

	/* validate */
	if ( path == NULL || path[ 0 ] == '\0' ) {
		Error("Empty path name given!");
	}

	/* add it */
	clean = Q_strdup( path );
	CleanPath( clean );

	// Note: this will add a message to the logs
	vfsInitDirectory(clean, qtrue /* find_paks */);
}


/*
   InitCommandLine()
   handles special options like -help, -game, -path, and common options like -v.
   will remove any arguments it uses.

   andrewj: merged InitPaths() code into here.
 */
static void InitCommandLine()
{
	int numPaths = 0;
	const char *str_value = NULL;

	while (! aj_arg_Done()) {
		// skip plain filenames here
		if (! aj_arg_CheckOption()) {
			aj_arg_Skip();
			continue;
		}

		if (aj_arg_Match("-help", "-h")) {
			const char *topic = NULL;
			if (! aj_arg_Done()) {
				topic = aj_arg_Remove();
			}
			HelpMain( topic );
			exit( 0 );
		}

		if (aj_arg_Match("-version", "")) {
			// nothing else needed, name and version already shown
			exit( 0 );
		}

		// verbose (show all messages on terminal)
		if ( MatchBool("-verbose", "-v", &verbose) )
			continue;

		// enable extra messages?
		if ( MatchBool("-extra", "-e", &extra) )
			continue;

		if ( MatchBool("-force", "", &forceBSP) )
			continue;

		if ( MatchBool("-leaktest", "", &leakTest) )
			continue;

		if ( MatchBool("-lumpswap", "", &lumpSwap) )
			continue;

		if ( aj_arg_MatchInt("-threads", "", &numthreads) )
			continue;

		if ( aj_arg_MatchString("-game", "-g", &str_value) ) {
			game = GetGame( str_value );
			if ( game == NULL ) {
				Error( "Unknown game: %s", str_value );
			}
			continue;
		}

		if ( aj_arg_MatchString("-path", "-p", &str_value) ) {
			AddPath( str_value );
			numPaths++;
			continue;
		}

		if (aj_arg_MatchString("-log", "", &log_override))
			continue;

		if (aj_arg_MatchString("-cfg", "", &cfg_override)) {
			cfgOverridden = qtrue;
			continue;
		}

		// check for missing value error, ignore unknown options
		const char *err_msg = aj_arg_GetError();
		if ( strstr(err_msg, "unknown option") != NULL ) {
			aj_arg_Skip();
			continue;
		}
		Error("%s\n", err_msg);
	}

	if ( numPaths == 0 ) {
		Sys_Warning( "No paths were specified.\n" );
	}

	// setup for future second pass
	aj_arg_Rewind();

	/* done */
	Sys_Printf( "\n" );
}


void ValidateSettings( void ) {
	ValidateBSPSettings();
	ValidateLightSettings();
	ValidateVisSettings();
}


/*
   ExitProgram()
   cleanup routine
 */
static void ExitProgram( void ) {
	BSPFilesCleanup();
	if ( mapDrawSurfs != NULL ) {
		free( mapDrawSurfs );
	}
}


/*
   main program.
   all your woes begin here.
 */
int main( int argc, const char **argv ) {
	int ret;
	double start, end;
	const char *filename = NULL;

#ifdef _WIN32
	_setmaxstdio(2048);
#endif

	/* we want consistent 'randomness' */
	srand( 0 );

	/* start timer */
	start = I_FloatTime();

	/* set exit call */
	atexit( ExitProgram );

	aj_arg_Init(argc, argv);

	if ( aj_arg_Done() ) {
		HelpMain( NULL );
		return 0;
	}

	/* set game to default (quake3) */
	game = &games[ 0 ];

	Sys_FPrintf( SYS_BIG, "AJMAP3 version %s (%s)\n", AJMAP_VERSION, __DATE__ );

	/* read general options first */
	InitCommandLine();

	/* init model library */
	PicoInit();
	PicoSetMallocFunc( safe_malloc );
	PicoSetFreeFunc( free );
	PicoSetPrintFunc( PicoPrintFunc );
	PicoSetLoadFileFunc( PicoLoadFileFunc );
	PicoSetFreeFileFunc( free );

	ThreadInit();

	InitBSPSettings();
	InitLightSettings();
	InitVisSettings();

	/* at this point, the command-line should be: [ command ] FILE */
	if ( aj_arg_Done() )
		Error( "Missing a command name or filename." );

	if ( aj_arg_CheckOption() ) {
		/* bsp */
		if ( aj_arg_MatchString("-bsp", "", &filename) ) {
			ret = BSPMain( filename );
		}

		/* light */
		else if ( aj_arg_MatchString("-light", "", &filename) ) {
			ret = LightMain( filename );
		}

		/* vis */
		else if ( aj_arg_MatchString("-vis", "", &filename) ) {
			ret = VisMain( filename );
		}

		/* fixaas */
		else if ( aj_arg_MatchString("-fixaas", "", &filename) ) {
			ret = FixAASMain( filename );
		}

		/* analyze */
		else if ( aj_arg_MatchString("-analyze", "", &filename) ||
				  aj_arg_MatchString("-analyse", "", &filename) ) {
			ret = AnalyzeBSPMain( filename );
		}

		/* info */
		else if ( aj_arg_MatchString("-info", "", &filename) ) {
			ret = BSPInfoMain( filename );
		}

		/* QBall: export entities */
		else if ( aj_arg_MatchString("-exportents", "", &filename) ) {
			ret = ExportEntitiesMain( filename );
		}

		/* ydnar: lightmap export */
		else if ( aj_arg_MatchString("-export", "", &filename) ) {
			ret = ExportLightmapsMain( filename );
		}

		/* ydnar: lightmap import */
		else if ( aj_arg_MatchString("-import", "", &filename) ) {
			ret = ImportLightmapsMain( filename );
		}

		else {
			// unknown command or missing filename
			const char *err_msg = aj_arg_GetError();
			Error("%s\n", err_msg);
		}

	} else {
		// no command name, assume -bsp
		ret = BSPMain( aj_arg_Remove() );
	}

	Sys_FPrintf( SYS_BIG, "\n" );

	/* emit time */
	end = I_FloatTime();
	Sys_Printf( "======== %1.0f seconds elapsed ========================================\n", end - start + 0.9);
	Sys_Printf( "\n" );

	/* return any error code */
	return ret;
}
