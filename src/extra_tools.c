/* -------------------------------------------------------------------------------

   Copyright (C) 1999-2013 id Software, Inc. and contributors.
   For a list of contributors, see the accompanying CONTRIBUTORS file.

   This file is part of GtkRadiant.

   GtkRadiant is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   GtkRadiant is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GtkRadiant; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

   ----------------------------------------------------------------------------------

   This code has been altered significantly from its original form, to support
   several games based on the Quake III Arena engine, in the form of "Q3Map2."

   ------------------------------------------------------------------------------- */



/* marker */
#define EXPORTENTS_C



/* dependencies */
#include "main.h"



/*
   ExportEntities()
   exports the entities to a text file (.ent)
 */

static void ExportEntities( void ) {
        char filename[MAX_OSPATH];
        FILE *file;

        /* note it */
        Sys_FPrintf( SYS_VRB, "--- ExportEntities ---\n" );

        /* do some path mangling */
        strcpy( filename, sourceFile );
        StripExtension( filename );
        strcat( filename, ".ent" );

        /* sanity check */
        if ( bspEntData == NULL || bspEntDataSize == 0 ) {
                Sys_Warning( "No BSP entity data. aborting...\n" );
                return;
        }

        /* write it */
        Sys_FPrintf( SYS_BIG, "Writing %s\n", filename );
        Sys_FPrintf( SYS_VRB, "(%d bytes)\n", bspEntDataSize );
        file = fopen( filename, "w" );

        if ( file == NULL ) {
                Error( "Unable to open %s for writing", filename );
        }

        fprintf( file, "%s\n", bspEntData );
        fclose( file );
}



/*
   ExportEntitiesMain()
   exports the entities to a text file (.ent)
 */

int ExportEntitiesMain( const char *filename ) {
	if (!filename)
		Error( "Missing filename for -exportents." );

	verbose = qtrue;

	/* do some path mangling */
	strcpy( sourceFile, ExpandArg( filename ) );
	StripExtension( sourceFile );
	DefaultExtension( sourceFile, ".bsp" );

	/* load the bsp */
	Sys_FPrintf( SYS_BIG, "Loading %s\n", sourceFile );
	LoadBSPFile( sourceFile );

	/* export the entities */
	ExportEntities();

	/* return to sender */
	return 0;
}


/*
   MD4BlockChecksum()
   calculates an md4 checksum for a block of data
 */

static int MD4BlockChecksum( void *buffer, int length ) {
	return Com_BlockChecksum( buffer, length );
}

/*
   FixAAS()
   resets an aas checksum to match the given BSP
 */

int FixAASMain( const char *filename ) {
	int length, checksum;
	void        *buffer;
	FILE        *file;
	char aas[MAX_OSPATH];
	const char  **ext;
	const char  *exts[] =
	{
		".aas",
		"_b0.aas",
		"_b1.aas",
		NULL
	};

	if (!filename)
		Error( "Missing filename for -fixaas." );

	verbose = qtrue;

	/* do some path mangling */
	strcpy( sourceFile, ExpandArg( filename ) );
	StripExtension( sourceFile );
	DefaultExtension( sourceFile, ".bsp" );

	/* note it */
	Sys_Printf( "--- FixAAS ---\n" );

	/* load the bsp */
	Sys_FPrintf( SYS_BIG, "Loading %s\n", sourceFile );
	length = LoadFile( sourceFile, &buffer );

	/* create bsp checksum */
	Sys_Printf( "Creating checksum...\n" );
	checksum = LittleLong( MD4BlockChecksum( buffer, length ) );

	/* write checksum to aas */
	ext = exts;
	while ( *ext )
	{
		/* mangle name */
		strcpy( aas, sourceFile );
		StripExtension( aas );
		strcat( aas, *ext );
		Sys_Printf( "Trying %s\n", aas );
		ext++;

		/* fix it */
		file = fopen( aas, "r+b" );
		if ( !file ) {
			continue;
		}
		if ( fwrite( &checksum, 4, 1, file ) != 1 ) {
			Error( "Error writing checksum to %s", aas );
		}
		fclose( file );
	}

	/* return to sender */
	return 0;
}



/*
   AnalyzeBSP() - ydnar
   analyzes a Quake engine BSP file
 */

typedef struct abspHeader_s
{
	char ident[ 4 ];
	int version;

	bspLump_t lumps[ 1 ];       /* unknown size */
}
abspHeader_t;

typedef struct abspLumpTest_s
{
	int radix, minCount;
	const char *name;
}
abspLumpTest_t;

int AnalyzeBSPMain( const char *filename ) {
	abspHeader_t            *header;
	int size, i, version, offset, length, lumpInt, count;
	char ident[ 5 ];
	void                    *lump;
	float lumpFloat;
	char lumpString[ 1024 ];
	abspLumpTest_t          *lumpTest;

	static abspLumpTest_t lumpTests[] =
	{
		{ sizeof( bspPlane_t ),         6,      "IBSP LUMP_PLANES" },
		{ sizeof( bspBrush_t ),         1,      "IBSP LUMP_BRUSHES" },
		{ 8,                            6,      "IBSP LUMP_BRUSHSIDES" },
		{ sizeof( bspBrushSide_t ),     6,      "RBSP LUMP_BRUSHSIDES" },
		{ sizeof( bspModel_t ),         1,      "IBSP LUMP_MODELS" },
		{ sizeof( bspNode_t ),          2,      "IBSP LUMP_NODES" },
		{ sizeof( bspLeaf_t ),          1,      "IBSP LUMP_LEAFS" },
		{ 104,                          3,      "IBSP LUMP_DRAWSURFS" },
		{ 44,                           3,      "IBSP LUMP_DRAWVERTS" },
		{ 4,                            6,      "IBSP LUMP_DRAWINDEXES" },
		{ 128 * 128 * 3,                1,      "IBSP LUMP_LIGHTMAPS" },
		{ 256 * 256 * 3,                1,      "IBSP LUMP_LIGHTMAPS (256 x 256)" },
		{ 512 * 512 * 3,                1,      "IBSP LUMP_LIGHTMAPS (512 x 512)" },
		{ 0, 0, NULL }
	};

	/* arg checking */
	if (!filename)
		Error( "Missing filename for -analyze." );

	verbose = qtrue;

	/* clean up map name */
	strcpy( sourceFile, ExpandArg( filename ) );
	Sys_FPrintf( SYS_BIG, "Loading %s\n", sourceFile );

	/* load the file */
	size = LoadFile( sourceFile, (void**) &header );
	if ( size == 0 || header == NULL ) {
		Error ( "Unable to load %s", sourceFile );
	}

	/* analyze ident/version */
	memcpy( ident, header->ident, 4 );
	ident[ 4 ] = '\0';
	version = LittleLong( header->version );

	Sys_Printf( "Identity:      %s\n", ident );
	Sys_Printf( "Version:       %d\n", version );
	Sys_Printf( "---------------------------------------\n" );

	/* analyze each lump */
	for ( i = 0; i < 100; i++ )
	{
		/* call of duty swapped lump pairs */
		if ( lumpSwap ) {
			offset = LittleLong( header->lumps[ i ].length );
			length = LittleLong( header->lumps[ i ].offset );
		}

		/* standard lump pairs */
		else
		{
			offset = LittleLong( header->lumps[ i ].offset );
			length = LittleLong( header->lumps[ i ].length );
		}

		/* extract data */
		lump = (byte*) header + offset;
		lumpInt = LittleLong( (int) *( (int*) lump ) );
		lumpFloat = LittleFloat( (float) *( (float*) lump ) );
		memcpy( lumpString, (char*) lump, ( (size_t)length < sizeof( lumpString ) ? (size_t)length : sizeof( lumpString ) - 1 ) );
		lumpString[ sizeof( lumpString ) - 1 ] = '\0';

		/* print basic lump info */
		Sys_Printf( "Lump:          %d\n", i );
		Sys_Printf( "Offset:        %d bytes\n", offset );
		Sys_Printf( "Length:        %d bytes\n", length );

		/* only operate on valid lumps */
		if ( length > 0 ) {
			/* print data in 4 formats */
			Sys_Printf( "As hex:        %08X\n", lumpInt );
			Sys_Printf( "As int:        %d\n", lumpInt );
			Sys_Printf( "As float:      %f\n", lumpFloat );
			Sys_Printf( "As string:     %s\n", lumpString );

			/* guess lump type */
			if ( lumpString[ 0 ] == '{' && lumpString[ 2 ] == '"' ) {
				Sys_Printf( "Type guess:    IBSP LUMP_ENTITIES\n" );
			}
			else if ( strstr( lumpString, "textures/" ) ) {
				Sys_Printf( "Type guess:    IBSP LUMP_SHADERS\n" );
			}
			else
			{
				/* guess based on size/count */
				for ( lumpTest = lumpTests; lumpTest->radix > 0; lumpTest++ )
				{
					if ( ( length % lumpTest->radix ) != 0 ) {
						continue;
					}
					count = length / lumpTest->radix;
					if ( count < lumpTest->minCount ) {
						continue;
					}
					Sys_Printf( "Type guess:    %s (%d x %d)\n", lumpTest->name, count, lumpTest->radix );
				}
			}
		}

		Sys_Printf( "---------------------------------------\n" );

		/* end of file */
		if ( offset + length >= size ) {
			break;
		}
	}

	/* last stats */
	Sys_Printf( "Lump count:    %d\n", i + 1 );
	Sys_Printf( "File size:     %d bytes\n", size );

	/* return to caller */
	return 0;
}



/*
   BSPInfo()
   emits statistics about the bsp file
 */

int BSPInfoMain( const char *filename ) {
	char ext[ 64 ];
	int size;
	FILE        *f;

	/* arg checking */
	if (!filename)
		Error( "Missing filename for -info." );

	verbose = qtrue;

	{
		/* mangle filename and get size */
		strcpy( sourceFile, filename );
		ExtractFileExtension( sourceFile, ext );
		if ( !Q_stricmp( ext, "map" ) ) {
			StripExtension( sourceFile );
		}
		DefaultExtension( sourceFile, ".bsp" );

		f = fopen( sourceFile, "rb" );
		if ( f ) {
			size = Q_filelength( f );
			fclose( f );
		} else {
			size = 0;
		}

		/* load the bsp file and print lump sizes */
		Sys_Printf( "-----------------------------------------\n" );
		Sys_Printf( "File: %s\n", sourceFile );
		Sys_Printf( "-----------------------------------------\n" );

		LoadBSPFile( sourceFile );
		PrintBSPFileSizes();

		/* print sizes */
		Sys_Printf( "\n" );
		Sys_Printf( "          total         %9d\n", size );
		Sys_Printf( "                        %9d KB\n", size / 1024 );
		Sys_Printf( "                        %9d MB\n", size / ( 1024 * 1024 ) );
		Sys_Printf( "\n" );
	}

	return 0;
}

