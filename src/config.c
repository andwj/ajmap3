/* -------------------------------------------------------------------------------

   Copyright (C) 1999-2007 id Software, Inc. and contributors.
   For a list of contributors, see the accompanying CONTRIBUTORS file.

   This file is part of GtkRadiant.

   GtkRadiant is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   GtkRadiant is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GtkRadiant; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

   ----------------------------------------------------------------------------------

   This code has been altered significantly from its original form, to support
   several games based on the Quake III Arena engine, in the form of "Q3Map2."

   ------------------------------------------------------------------------------- */


/* dependencies */
#include "main.h"


/* andrewj 2019/2020: implemented this system for parsing config settings */

typedef enum
{
	CFG_END    = 0,  // marks end of the list
	CFG_BOOL   = 1,  // plain keyword sets as qtrue
	CFG_NEGATE = 2,  // inverse of CFG_BOOL, plain keyword sets as qfalse

	CFG_INT    = 3,  // 32-bit signed integer
	CFG_FLOAT  = 4,  // 32-bit floating point
	CFG_DOUBLE = 5,  // 64-bit floating point

	CFG_QPATH  = 7,  // a MAX_QPATH string
	CFG_OSPATH = 8,  // a MAX_OSPATH filename
	CFG_VEC3   = 9,  // a vec3_t value
} cfgfieldtype_e;


typedef struct
{
	cfgfieldtype_e type;	// type of field
	const char *name;		// name of the field
	const char *alias;		// alternate name, "" if none
	size_t offset;			// offset in the structure
	const char *help;		// help text for option, NULL to skip
} cfgfielddef_t;


#define BS_OFS(x)  (size_t)&(((bsp_settings_t *)0)->x)
#define LS_OFS(x)  (size_t)&(((light_settings_t *)0)->x)
#define VS_OFS(x)  (size_t)&(((vis_settings_t *)0)->x)


static cfgfielddef_t bsp_fields[] =
{
	{ CFG_BOOL,   "altsplit", "", BS_OFS(alternateSplitWeights),
		"Alternate BSP tree splitting weights (should give more fps)" },
	{ CFG_QPATH,  "celShader", "", BS_OFS(globalCelShader),
		"Sets a global cel shader name" },
	{ CFG_FLOAT,  "clipDepth", "", BS_OFS(clipDepthGlobal),
		"Sets the global model autoclip thickness" },
	{ CFG_BOOL,   "custInfoParms", "", BS_OFS(useCustomInfoParms),
		"Read scripts/custinfoparms.txt"},
	{ CFG_BOOL,   "debugClip", "", BS_OFS(debugClip),
		"Add debug surfaces for the clipping brushes of 3D models inserted into the map" },
	{ CFG_BOOL,   "debugInset", "", BS_OFS(debugInset),
		"Push all triangle vertexes towards the triangle center" },
	{ CFG_BOOL,   "debugPortals", "", BS_OFS(debugPortals),
		"Make BSP portals visible in the map" },
	{ CFG_BOOL,   "deep", "", BS_OFS(deep),
		"Use detail brushes in the BSP tree, but at lowest priority (should give more fps)" },
	{ CFG_DOUBLE, "distEpsilon", "de", BS_OFS(distanceEpsilon),
		"Distance epsilon for plane snapping etc." },
	{ CFG_BOOL,   "flares", "", BS_OFS(emitFlares),
		"Turn on support for flares" },
	{ CFG_NEGATE, "noFlares", "", BS_OFS(emitFlares),
		"Turn off support for flares" },
	{ CFG_BOOL,   "flat", "", BS_OFS(flat),
		"Enable flat shading (good for combining with -celshader)" },
	{ CFG_BOOL,   "fullDetail", "", BS_OFS(fulldetail),
		"Treat detail brushes as structural ones" },
	{ CFG_BOOL,   "maxArea", "", BS_OFS(maxAreaFaceSurface),
		"Enable Max Area face surface generation" },
	{ CFG_INT,    "maxLMSurfaceVerts", "mv", BS_OFS(maxLMSurfaceVerts),
		"Sets the maximum number of vertices of a lightmapped surface" },
	{ CFG_INT,    "maxSurfaceIndexes", "mi", BS_OFS(maxSurfaceIndexes),
		"Sets the maximum number of indexes per surface" },
	{ CFG_BOOL,   "meta", "", BS_OFS(meta),
		"Combine adjacent triangles of the same texture to surfaces" },
	{ CFG_BOOL,   "noCurves", "", BS_OFS(noCurveBrushes),
		"Turn off support for patches" },
	{ CFG_BOOL,   "noDetail", "", BS_OFS(nodetail),
		"Leave out detail brushes" },
	{ CFG_BOOL,   "noFog", "", BS_OFS(nofog),
		"Turn off support for fog volumes" },
	{ CFG_BOOL,   "noHint", "", BS_OFS(noHint),
		"Turn off support for hint brushes" },
	{ CFG_BOOL,   "noOb", "", BS_OFS(noob),
		"No oBs!" },
	{ CFG_DOUBLE, "normalEpsilon", "ne", BS_OFS(normalEpsilon),
		"Normal epsilon for plane snapping etc." },
	{ CFG_BOOL,   "noSubdivide", "", BS_OFS(nosubdivide),
		"Turn off support for `q3map_tessSize` (breaks water vertex deforms)" },
	{ CFG_BOOL,   "noTjunc", "", BS_OFS(notjunc),
		"Do not fix T-junctions (causes cracks between triangles, do not use)" },
	{ CFG_BOOL,   "noWater", "", BS_OFS(nowater),
		"Turn off support for water, slime or lava" },
	{ CFG_FLOAT,  "npAngle", "np", BS_OFS(npDegrees),
		"Force all surfaces to be nonplanar with a given shade angle" },
	{ CFG_BOOL,   "patchMeta", "", BS_OFS(patchMeta),
		"Turn patches into triangle meshes for display" },
	{ CFG_INT,    "subdivisions", "", BS_OFS(patchSubdivisions),
		"Multiply factor for number of patch subdivisions" },
	{ CFG_BOOL,   "rename", "", BS_OFS(renameModelShaders),
		"Append suffix to miscmodel shaders (needed for SoF2)" },
	{ CFG_BOOL,   "skyFix", "", BS_OFS(skyFixHack),
		"Turn sky box into six surfaces to work around ATI problems" },
	{ CFG_INT,    "snap", "", BS_OFS(bevelSnap),
		"Snap brush bevel planes to the given number of units" },
	{ CFG_INT,    "texRange", "", BS_OFS(texRange),
		"Limit per-surface texture range to the given number of units, and subdivide surfaces like with `q3map_tessSize` if this is not met" },
	{ CFG_BOOL,   "verboseEntities", "", BS_OFS(verboseEntities),
		"Enable `-v` only for map entities, not for the world" },

	/* TODO : keep these?  if so, describe them.
	{ CFG_INT,    "metaAdequateScore", "", BS_OFS(metaAdequateScore),
		"" },
	{ CFG_INT,    "metaGoodScore", "", BS_OFS(metaGoodScore),
		"" },
	{ CFG_FLOAT,  "metaMaxBboxDist", "", BS_OFS(metaMaxBBoxDistance),
		"" },
	*/

	/* NOT HERE:
         maxSurfaceVerts : I reckon this is probably not worth having a
		                   setting for it, plus it seems to only affect
						   meta surfaces (only used in surface_meta.c).
	 */

	// the end
	{ CFG_END, NULL, NULL, 0, NULL }
};


static cfgfielddef_t light_fields[] =
{
	// TODO emulate this {"-nolightmapsearch", "Do not optimize lightmap packing for GPU memory usage (as doing so costs fps)" },
	//      the arg parser sets LS.lightmapSearchBlockSize to 1
	//      { CFG_INT,    "lightmapSearchBlockSize", "", LS_OFS(lightmapSearchBlockSize),


	{ CFG_INT,    "approx", "", LS_OFS(approximateTolerance),
		"Vertex light approximation tolerance (never use in conjunction with deluxemapping)" },
	{ CFG_FLOAT,  "surface", "surfaceScale", LS_OFS(faceScale),
		"Scaling factor for surface (diffuse) lights" },
	{ CFG_FLOAT,   "backsplash", "", LS_OFS(backsplashFraction),
		"Backsplash scale factor for surface lights: 1.0 is normal, can be higher or lower" },
	{ CFG_FLOAT,   "backsplashDist", "", LS_OFS(backsplashDistance),
		"Backsplash distance (default is 23 units)" },
	{ CFG_INT,    "bounce", "", LS_OFS(bounce),
		"Number of bounces for radiosity (default is none)" },
	{ CFG_FLOAT,  "bounceColorRatio", "", LS_OFS(bounceColorRatio),
		"Color ratio for bounces (0.0 to 1.0), higher gives more color from textures" },
	{ CFG_BOOL,   "bounceNoGrid", "", LS_OFS(bounceNoGrid),
		"Do not compute radiosity on the light grid" },
	{ CFG_BOOL,   "bounceOnly", "", LS_OFS(bounceOnly),
		"Only compute radiosity" },
	{ CFG_FLOAT,  "bounceScale", "", LS_OFS(bounceScale),
		"Scaling factor for radiosity" },
	{ CFG_FLOAT,  "brightness", "", LS_OFS(lightmapBrightness),
		"Lightmap brightness" },
	{ CFG_FLOAT,  "contrast", "", LS_OFS(lightmapContrast),
		"Lightmap contrast" },
	{ CFG_FLOAT,  "gamma", "", LS_OFS(lightmapGamma),
		"Lightmap gamma" },
	{ CFG_BOOL,   "cheap", "", LS_OFS(cheap),
		"Abort vertex light calculations when white is reached" },
	{ CFG_BOOL,   "cheapGrid", "", LS_OFS(cheapgrid),
		"Use `-cheap` style lighting for radiosity" },
	{ CFG_FLOAT,  "compensate", "", LS_OFS(lightmapCompensate),
		"Lightmap compensate (darkening factor applied after everything else)" },
	{ CFG_BOOL,   "cpma", "", LS_OFS(cpmaHack),
		"Challenge ProMode Asstacular Vertex Light Mode (tm)" },
	{ CFG_BOOL,   "dark", "", LS_OFS(dark),
		"Darken lightmap seams" },
	{ CFG_BOOL,   "debug", "", LS_OFS(debug),
		"Mark the lightmaps according to the cluster: unmapped clusters get yellow, occluded ones get pink, flooded ones get blue overlay color, otherwise red" },
	{ CFG_BOOL,   "debugAxis", "", LS_OFS(debugAxis),
		"Color the lightmaps according to the lightmap axis" },
	{ CFG_BOOL,   "debugBorder", "", LS_OFS(lightmapBorder),
		"Add a red border to lightmaps for debugging" },
	{ CFG_BOOL,   "debugDeluxe", "", LS_OFS(debugDeluxemap),
		"Show deluxemaps on the lightmap" },
	{ CFG_BOOL,   "debugDirt", "", LS_OFS(debugDirt),
		"Store the dirtmaps as lightmaps for debugging" },
	{ CFG_BOOL,   "debugNormals", "", LS_OFS(debugNormals),
		"Color the lightmaps according to the direction of the surface normal" },
	{ CFG_BOOL,   "debugOrigin", "", LS_OFS(debugOrigin),
		"Color the lightmaps according to the origin of the luxels" },
	{ CFG_INT,    "debugSampleSize", "", LS_OFS(debugSampleSize),
		"Print warnings when surfaces are too large for samplesize/lightmapsize/lightmapscale combination." },
	{ CFG_BOOL,   "debugSurfaces", "", LS_OFS(debugSurfaces),
		"Color surfaces according to their lightmap index" },
	{ CFG_BOOL,   "deluxe", "deluxMap", LS_OFS(deluxemap),
		"Enable deluxemapping (light direction maps)" },
	{ CFG_NEGATE, "noDeluxe", "noDeluxMap", LS_OFS(deluxemap),
		"Disable deluxemapping" },
	{ CFG_INT,    "deluxeMode", "", LS_OFS(deluxeMode),
		"Deluxemapping mode: 0 = Modelspace (darkplaces), 1 = Tangentspace" },
	{ CFG_BOOL,   "dirty", "dirtMap", LS_OFS(dirty),
		"Enable dirtmapping" },
	{ CFG_INT,    "dirtMode", "", LS_OFS(dirtMode),
		"Dirtmapping mode: 0 = Ordered direction, 1 = Randomized direction" },
	{ CFG_FLOAT,  "dirtDepth", "", LS_OFS(dirtDepth),
		"Dirtmapping depth" },
	{ CFG_FLOAT,  "dirtGain", "", LS_OFS(dirtGain),
		"Dirtmapping exponent" },
	{ CFG_FLOAT,  "dirtScale", "", LS_OFS(dirtScale),
		"Dirtmapping scaling factor" },
	{ CFG_BOOL,   "dump", "", LS_OFS(dump),
		"Dump radiosity from `-bounce` into numbered MAP file prefabs" },
	{ CFG_BOOL,   "export", "", LS_OFS(exportLightmaps),
		"Export lightmaps when compile finished" },
	{ CFG_FLOAT,  "exposure", "", LS_OFS(lightmapExposure),
		"Lightmap exposure to better support overbright spots" },
	{ CFG_BOOL,   "external", "", LS_OFS(externalLightmaps),
		"Force external lightmaps even if at size of internal lightmaps" },
	{ CFG_FLOAT,  "extraDist", "", LS_OFS(extraDist),
		"Set extra distance for point light calculations" },
	{ CFG_BOOL,   "extraVisNudge", "", LS_OFS(lightmapExtraVisClusterNudge),
		"Broken feature to nudge the luxel origin to a better vis cluster" },
	{ CFG_BOOL,   "fast", "", LS_OFS(fast),
		"Ignore tiny light contributions" },
	{ CFG_BOOL,   "fastBounce", "", LS_OFS(fastbounce),
		"Use `-fast` style lighting for radiosity" },
	{ CFG_BOOL,   "faster", "", LS_OFS(faster),
		"Use a faster falloff curve for lighting; also implies `-fast`" },
	{ CFG_BOOL,   "fastGrid", "", LS_OFS(fastgrid),
		"Use `-fast` style lighting for the light grid" },
	{ CFG_BOOL,   "fill", "", LS_OFS(lightmapFill),
		"Fill lightmap colors from surrounding pixels to improve JPEG compression" },
	{ CFG_BOOL,   "filter", "", LS_OFS(filter),
		"Lightmap filtering" },
	{ CFG_BOOL,   "floodlight", "", LS_OFS(floodlight),
		"Enable floodlight (zero-effort somewhat decent lighting)" },
	{ CFG_VEC3,   "floodColor", "", LS_OFS(floodlightColor),
		"Floodlight global color, default is white" },
	{ CFG_FLOAT,  "floodDist", "", LS_OFS(floodlightDistance),
		"Floodlight distance to trace for walls and sky, higher values make darker levels, default is 1024" },
	{ CFG_FLOAT,  "floodDirectionScale", "", LS_OFS(floodlightDirectionScale),
		"Floodlight direction scale (only used for deluxemapping)" },
	{ CFG_FLOAT,  "floodIntensity", "", LS_OFS(floodlightIntensity),
		"Floodlight intensity, default is 128" },
	{ CFG_FLOAT,  "gridAmbientBoost", "", LS_OFS(gridAmbientBoost),
		"Boost amount for the light grid ambient components (0 to 255)" },
	{ CFG_FLOAT,  "gridAmbientScale", "", LS_OFS(gridAmbientScale),
		"Scaling factor for the light grid ambient components" },
	{ CFG_FLOAT,  "gridDirBoost", "", LS_OFS(gridDirBoost),
		"Boost amount for the light grid directional components (0 to 255)" },
	{ CFG_FLOAT,  "gridDirScale", "", LS_OFS(gridDirScale),
		"Scaling factor for the light grid directional components" },
	{ CFG_FLOAT,  "gridDirectionality", "", LS_OFS(gridDirectionality),
		"Directionality factor for the light grid: 1.0 is normal, 0.0 is ambient only" },
	{ CFG_BOOL,   "gridForceDown", "", LS_OFS(gridForceDown),
		"For the directional component in the light grid to be downwards" },
	{ CFG_BOOL,   "halfLambert", "", LS_OFS(lightAngleHL),
		"Enable half lambert light angle attenuation" },
	{ CFG_BOOL,   "keepLights", "", LS_OFS(keepLights),
		"Keep light entities in the BSP file after compile" },
	{ CFG_OSPATH, "lightmapDir", "", LS_OFS(lmCustomDir),
		"Directory for external lightmaps (implies `-external`)" },
	{ CFG_INT,    "minSampleSize", "", LS_OFS(minSampleSize),
		"Sets minimum lightmap resolution in luxels/qu" },
	{ CFG_BOOL,   "noCollapse", "", LS_OFS(noCollapse),
		"Do not collapse identical lightmaps" },
	{ CFG_BOOL,   "noGrid", "", LS_OFS(noGridLighting),
		"Disable grid light calculation (makes all entities fullbright)" },
	{ CFG_NEGATE,   "noFastPoint", "", LS_OFS(fastpoint),
		"Disable automatic fast mode for point lights" },
	{ CFG_BOOL,   "noLm", "", LS_OFS(nolm),
		"No lightmaps yo!" },
	{ CFG_BOOL,   "noShaderSun", "", LS_OFS(noShaderSun),
		"Disable sun lighting from any sky shaders" },
	{ CFG_BOOL,   "noSurf", "", LS_OFS(noSurfaces),
		"Disable tracing against surfaces (only uses BSP nodes then)" },
	{ CFG_BOOL,   "noTrace", "", LS_OFS(noTrace),
		"Disable shadow occlusion" },
	{ CFG_BOOL,   "noVertex", "", LS_OFS(noVertexLighting),
		"Disable vertex lighting calculation (use a constant level)" },
	{ CFG_BOOL,   "patchShadows", "", LS_OFS(patchShadows),
		"Cast shadows from patches" },
	{ CFG_FLOAT,  "point", "pointScale", LS_OFS(pointScale),
		"Scaling factor for entity point lights" },
	{ CFG_BOOL,   "randomSamples", "", LS_OFS(lightRandomSamples),
		"Random sampling" },
	{ CFG_BOOL,   "randomSamplesInsist", "", LS_OFS(lightRandomInsist),
		"When random sampling, honor `-samples` as the number of random samples per texel" },
	{ CFG_INT,    "rawLightmapSizeLimit", "", LS_OFS(lmLimitSize),
		"Sets the raw lightmap size limit" },
	{ CFG_INT,    "samples", "", LS_OFS(lightSamples),
		"Adaptive supersampling quality" },
	{ CFG_INT,    "sampleScale", "", LS_OFS(sampleScale),
		"Scales all lightmap resolutions" },
	{ CFG_INT,    "sampleSize", "", LS_OFS(sampleSize),
		"Sets default lightmap resolution in luxels/qu" },
	{ CFG_INT,    "samplesSearchBoxSize", "", LS_OFS(lightSamplesSearchBoxSize),
		"Adaptive supersampling will use this multiple of box size (1-4)" },
	{ CFG_FLOAT,  "scale", "", 0 /* handled specially */,
		"Scaling factor for all lightmap lights" },
	{ CFG_BOOL,   "shade", "", LS_OFS(shade),
		"Enable phong shading at default shade angle" },
	{ CFG_FLOAT,  "shadeAngle", "", LS_OFS(shadeAngleDegrees),
		"Angle for phong shading" },
	{ CFG_FLOAT,  "sky", "skyScale", LS_OFS(skyScale),
		"Scaling factor for sky and sun light" },
	{ CFG_BOOL,   "smooth", "", 0 /* handled specially */,
		"Backwards compatibility alias for `-samples 2`" },
	{ CFG_FLOAT,  "spot", "spotScale", LS_OFS(spotScale),
		"Scaling factor for entity spot lights" },
	{ CFG_BOOL,   "sunOnly", "", LS_OFS(sunOnly),
		"Only compute sun light" },
	{ CFG_INT,    "super", "superSample", LS_OFS(superSample),
		"Ordered grid supersampling quality" },
	{ CFG_BOOL,   "sRGB", "", 0 /* handled specially */,
		"Colorspace is sRGB (_color, lightmaps, textures)" },
	{ CFG_BOOL,   "sRGBcolor", "", LS_OFS(color_sRGB),
		"Colorspace for _color is sRGB" },
	{ CFG_BOOL,   "sRGBlight", "", LS_OFS(lightmap_sRGB),
		"Colorspace for lightmaps is sRGB" },
	{ CFG_BOOL,   "sRGBtex",   "", LS_OFS(texture_sRGB),
		"Colorspace for textures is sRGB" },
	{ CFG_NEGATE, "nosRGBcolor", "", LS_OFS(color_sRGB),
		"Colorspace for _color is linear" },
	{ CFG_NEGATE, "nosRGBlight", "", LS_OFS(lightmap_sRGB),
		"Colorspace for lightmaps is linear" },
	{ CFG_NEGATE, "nosRGBtex",   "", LS_OFS(texture_sRGB),
		"Colorspace for textures is linear" },
	{ CFG_FLOAT,  "thresh", "", LS_OFS(subdivideThreshold),
		"Triangle subdivision threshold (default is 1.0)" },
	{ CFG_BOOL,   "triangleCheck", "", LS_OFS(lightmapTriangleCheck),
		"Broken check that should ensure luxels apply to the right triangle" },
	{ CFG_BOOL,   "triSoup", "", LS_OFS(trisoup),
		"Convert brush faces to triangle soup" },
	{ CFG_FLOAT,  "vertexScale", "", LS_OFS(vertexScale),
		"Scaling factor for vertex lights" },
	{ CFG_NEGATE, "q3", "", LS_OFS(wolfLight),
		"Use non-linear falloff curve by default (like Q3A)" },
	{ CFG_BOOL,   "wolf", "", LS_OFS(wolfLight),
		"Use linear falloff curve by default (like W:ET)" },

	/* TODO : keep these? previous code had no argument parsing for them.
	          would need to figure out what they do and describe them.
			  plus LightMain set some of them (would override any user value).

	{ CFG_BOOL,   "exactPointToPolygon", "", LS_OFS(exactPointToPolygon),
	{ CFG_FLOAT,  "falloffTolerance", "", LS_OFS(falloffTolerance),
	{ CFG_FLOAT,  "linearScale", "", LS_OFS(linearScale),
	{ CFG_FLOAT,  "formFactorValueScale", "", LS_OFS(formFactorValueScale),
	*/

	// the end
	{ CFG_END, NULL, NULL, 0, NULL }
};


static cfgfielddef_t vis_fields[] =
{
	{ CFG_BOOL, "debugCluster", "", VS_OFS(debugCluster),
		"Write some debugging info about clusters" },
	{ CFG_BOOL, "fast", "", VS_OFS(fast),
		"Very fast and crude vis calculation" },
	{ CFG_BOOL, "noPassage", "", VS_OFS(noPassage),
		"Just use PortalFlow vis (usually less fps)" },
	{ CFG_BOOL, "passageOnly", "", VS_OFS(passageOnly),
		"Just use PassageFlow vis (usually less fps)" },
	{ CFG_BOOL, "merge", "", VS_OFS(merge),
		"Faster but still okay vis calculation" },
	{ CFG_BOOL, "mergePortals", "", VS_OFS(mergeportals),
		"The less crude half of `-merge`, makes vis sometimes much faster but doesn't hurt fps usually" },
	{ CFG_BOOL, "noSort", "", VS_OFS(nosort),
		"Do not sort the portals before calculating vis (usually slower)" },
	{ CFG_BOOL, "hint", "", VS_OFS(hint),
		"Merge all but hint portals (implies -merge)" },

	// the end
	{ CFG_END, NULL, NULL, 0, NULL }
};


static const char * ConfigSectionName(char section) {
	switch (section) {
	case 'B': return "bsp";
	case 'L': return "light";
	case 'V': return "vis";
	default:  return "XXX";
	}
}


static const cfgfielddef_t * ConfigLookup(char section, const char *name) {
	const cfgfielddef_t *field;

	switch (section) {
	case 'B': field = &bsp_fields[0];   break;
	case 'L': field = &light_fields[0]; break;
	case 'V': field = &vis_fields[0];   break;
	default: return NULL;
	}

	for ( ; field->type != CFG_END ; field++) {
		if (Q_stricmp(field->name, name) == 0)
			return field;

		if (field->alias && field->alias[0]) {
			if (Q_stricmp(field->alias, name) == 0)
				return field;
		}
	}

	// not found
	return NULL;
}


static void ConfigParseSetting(char section, const cfgfielddef_t *field, const char *value) {
	// if value should be numeric but begins with a letter, it is probably a
	// missing value.
	if (field->type == CFG_INT || field->type == CFG_FLOAT ||
		field->type == CFG_DOUBLE || field->type == CFG_VEC3) {

		if (isalpha(value[0]))
			Error("bad or missing value for '%s' in cfg file.", field->name);
	}

	/* handle special keywords */

	if (Q_stricmp(field->name, "sRGB") == 0) {
		LS.color_sRGB = qtrue;
		LS.lightmap_sRGB = qtrue;
		LS.texture_sRGB = qtrue;
		return;
	}

	if (Q_stricmp(field->name, "smooth") == 0) {
		LS.lightSamples = 2;
		return;
	}

	if (Q_stricmp(field->name, "scale") == 0) {
		float val = atof(value);
		LS.pointScale = val;
		LS.spotScale = val;
		LS.faceScale = val;
		LS.skyScale = val;
		LS.bounceScale = val;
		return;
	}

	/* parse value based on type of field */

	char *base_ptr = NULL;
	switch (section) {
	case 'B': base_ptr = (char *) &BS; break;
	case 'L': base_ptr = (char *) &LS; break;
	case 'V': base_ptr = (char *) &VS; break;
	default: break;
	}

	switch (field->type) {
	case CFG_BOOL: {
		qboolean *var = (qboolean *)(base_ptr + field->offset);
		*var = (value[0] != '0') ? qtrue : qfalse;
		break;
	}
	case CFG_NEGATE: {
		qboolean *var = (qboolean *)(base_ptr + field->offset);
		*var = (value[0] == '0') ? qtrue : qfalse;
		break;
	}
	case CFG_INT: {
		int *var = (int *)(base_ptr + field->offset);
		*var = atoi( value );
		break;
	}
	case CFG_FLOAT: {
		float *var = (float *)(base_ptr + field->offset);
		*var = atof( value );  // result is double
		break;
	}
	case CFG_DOUBLE: {
		double *var = (double *)(base_ptr + field->offset);
		*var = atof( value );  // result is double
		break;
	}
	case CFG_QPATH: {
		char *buf = (char *)(base_ptr + field->offset);
		if (strlen(value) > MAX_QPATH - 1) {
			Error( "Bad value for '%s', too long (over %d chars)", field->name, MAX_QPATH - 1 );
		}
		strcpy(buf, value);
		break;
	}
	case CFG_OSPATH: {
		char *buf = (char *)(base_ptr + field->offset);
		if (strlen(value) > MAX_OSPATH - 1) {
			Error( "Bad value for '%s', too long (over %d chars)", field->name, MAX_OSPATH - 1 );
		}
		strcpy(buf, value);
		break;
	}
	case CFG_VEC3: {
		// this should be OK as vec3_t is just vec_t[3]
		vec_t *v = (vec_t *)(base_ptr + field->offset);
		if (sscanf(value, " %f %f %f ", &v[0], &v[1], &v[2]) != 3) {
			Error( "Bad value for '%s', wanted 3 floats, got: '%s'", field->name, value );
		}
		break;
	}
	default:
		break;
	}
}


void LoadCfgFile(const char *filename) {
	void *buffer;
	const char *text_p;

	const char *token;
	const char *pending = NULL;

	char section = 0;  // 'B' or 'L' or 'V' or 'S'
	int bracket_level = 0;

	// has it been disabled?
	if (!filename || strlen(filename) == 0)
		return;

	// an implicit cfg file is allowed to not exist
	if (!cfgOverridden && !FileExists(filename))
		return;

	// this will Error() if the file does not exist
	LoadFile(filename, &buffer);
	text_p = buffer;

	Sys_Printf("Reading cfg file %s\n", filename);

	while (text_p || pending) {
		const char *keyword;
		const char *value;
		const cfgfielddef_t *field;

		if (pending != NULL) {
			token = pending;
			pending = NULL;
		} else {
			token = COM_Parse(&text_p);
		}

		if (Q_stricmp(token, "{") == 0) {
			bracket_level++;
			continue;
		}
		else if (Q_stricmp(token, "}") == 0) {
			bracket_level--;

			if (bracket_level == 0)
				section = 0;
			else if (bracket_level < 0)
				Error("mismatched {} in cfg file.");

			continue;
		}

		if (bracket_level == 0 && Q_stricmp(token, "bsp") == 0) {
			section = 'B';
			continue;
		}
		else if (bracket_level == 0 && Q_stricmp(token, "light") == 0) {
			section = 'L';
			continue;
		}
		else if (bracket_level == 0 && Q_stricmp(token, "vis") == 0) {
			section = 'V';
			continue;
		}
		else if (bracket_level == 0 && Q_stricmp(token, "shaders") == 0) {
			section = 'S';
			continue;
		}

		if (! (bracket_level == 1 && section != 0))
			continue;

		// shader filenames are single tokens
		if (section == 'S') {
			AddShaderFile( token );
			continue;
		}

		/* parse a single setting */

		keyword = Q_strdup(token);

		if (! isalpha(keyword[0]))
			Error("bad setting name '%s' in cfg file.", keyword);

		// look up the setting name
		field = ConfigLookup(section, keyword);

		if (field == NULL) {
			Error("unknown %s setting '%s' in cfg file.", ConfigSectionName(section), keyword);
		}

		// for booleans, a following value must be "0" or "1" or omitted.
		// for every other type, a following value is compulsory.

		value = NULL;

		if (text_p != NULL) {
			token = COM_Parse(&text_p);
			value = token; // can be NULL

			if (field->type == CFG_BOOL || field->type == CFG_NEGATE) {
				if (token && ! (token[0] == '0' || token[0] == '1')) {
					// push the token back (like ungetc)
					pending = token;
					value = "1";
				}
			}
		}

		if (value == NULL || value[0] == 0 || Q_stricmp(value, "}") == 0)
			Error("missing value for '%s' in cfg file.", keyword);

		ConfigParseSetting(section, field, value);

		free( (void *) keyword );
	}

	free( buffer );
}


//----------------------------------------------------------------------

static cfgfielddef_t help_common_options[] =
{
	{ CFG_INT, "-game <name>", "",   0, "Load settings for the given game (default: quake3)" },
	{ CFG_INT, "-path <dir>", "",    0, "Specify a package directory (can be used more than once to look in multiple paths)" },
	{ CFG_INT, "-log <file>", "",    0, "Override the log filename, \"none\" to disable it" },
	{ CFG_INT, "-cfg <file>", "",    0, "Override the config filename" },
	{ CFG_INT, "-threads <num>", "", 0, "Number of threads to use" },
	{ CFG_INT, "", "",               0, "" },
	{ CFG_INT, "-verbose, -v", "",   0, "Verbose output on the terminal" },
	{ CFG_INT, "-extra", "",         0, "Enable messages with extra details" },
	{ CFG_INT, "-leaktest", "",      0, "When building a BSP, a leak is a fatal error" },
	{ CFG_INT, "-force", "",         0, "Allow reading some broken/unsupported BSP files e.g. when decompiling, may also crash" },
	{ CFG_INT, "-help [topic]", "",  0, "Display help text for a topic or command. The special topics are: 'options', 'games' and 'commands'" },

	{ CFG_END, NULL, NULL, 0, NULL }
};


static cfgfielddef_t help_command_list[] =
{
	{ CFG_INT, "-bsp", "",        0, "BSP generation" },
	{ CFG_INT, "-light", "",      0, "Light processing" },
	{ CFG_INT, "-vis", "",        0, "VIS calculation" },
	{ CFG_INT, "", "",            0, "" },
	{ CFG_INT, "-import", "",     0, "Importing lightmaps" },
	{ CFG_INT, "-export", "",     0, "Exporting lightmaps" },
	{ CFG_INT, "-exportents", "", 0, "Exporting entities" },
	{ CFG_INT, "-fixaas", "",     0, "Fixing AAS checksum" },
	{ CFG_INT, "-analyze", "",    0, "Analyzing BSP-like file structure" },
	{ CFG_INT, "-info", "",       0, "Get info about BSP file" },

	{ CFG_END, NULL, NULL, 0, NULL }
};


static void FormatHelpInfo(const char *indent, const char *name, const char *help) {
	int width = 76;
	int printed = printf("%s%-20s  ", indent, name);
	int length = strlen(help);
	int j = 0;

	while ( j < length && length-j > width - printed ) {
		if ( j != 0 ) {
			printf("%s%22c", indent, ' ');
		}
		int fragment = width - printed;
		while ( fragment > 0 && help[j+fragment-1] != ' ') {
			fragment--;
		}
		j += fwrite(help+j, sizeof(char), fragment, stdout);
		putchar('\n');
		printed = 22;
	}
	if ( j == 0 ) {
		printf("%s\n", help+j);
	}
	else if ( j < length ) {
		printf("%s%22c%s\n", indent, ' ', help+j);
	}
}


static void ConfigShowHelp(const char *title, const cfgfielddef_t *options) {
	printf("%s\n", title);

	int i;
	for ( i = 0 ; options[i].type != CFG_END ; i++ ) {
		if (options[i].help != NULL)
			FormatHelpInfo("  ", options[i].name, options[i].help);
	}

	putchar('\n');
}


static void ShowAvailableGames( void ) {
	int i;

	printf("Available games:\n");

	for (i = 0; games[i].arg != NULL; i++) {
		printf("  %s\n", games[i].arg);
	}
}


void HelpMain(const char *arg) {
	if (arg == NULL)
		arg = "";

	// remove any '-' from start of topic name
	while (arg[0] == '-')
		arg++;

	if (arg[0] == 0 || Q_stricmp(arg, "options") == 0) {
		printf("Usage:\n");
		printf("  ajmap3 <command> FILE [OPTIONS...]\n");
		printf("\n");

		ConfigShowHelp("General options:", help_common_options);
		return;
	}

	if (Q_stricmp(arg, "commands") == 0 || Q_stricmp(arg, "cmds") == 0 ) {
		ConfigShowHelp("Available commands:", help_command_list);
		return;
	}
	if (Q_stricmp(arg, "games") == 0) {
		ShowAvailableGames();
		return;
	}
	if (Q_stricmp(arg, "bsp") == 0) {
		ConfigShowHelp("BSP settings:", bsp_fields);
		return;
	}
	if (Q_stricmp(arg, "light") == 0) {
		ConfigShowHelp("Light settings:", light_fields);
		return;
	}
	if (Q_stricmp(arg, "vis") == 0) {
		ConfigShowHelp("Vis settings:", vis_fields);
		return;
	}

	if (Q_stricmp(arg, "export") == 0) {
		printf("Usage:\n");
		printf("  ajmap3 -export <file.bsp>\n");
		printf("\n");

		printf("Copies lightmaps from the BSP to `file/lightmap_####.tga` files,\n");
		printf("where #### is a numeric id starting at zero.  Note that in Quake3,\n");
		printf("each lightmap is 128x128 luxels and can contain multiple surfaces.\n");
		return;
	}

	if (Q_stricmp(arg, "import") == 0) {
		printf("Usage:\n");
		printf("  ajmap3 -import <file.bsp>\n");
		printf("\n");

		printf("Copies lightmaps from `file/lightmap_####.tga` files into the BSP,\n");
		printf("where #### is a numeric id starting at zero.  Image files which are\n");
		printf("absent are skipped with a warning.  Note that in Quake3, each\n");
		printf("lightmap is 128x128 luxels and can contain multiple surfaces.\n");
		return;
	}

	if (Q_stricmp(arg, "exportents") == 0) {
		printf("Usage:\n");
		printf("  ajmap3 -exportents <file.bsp>\n");
		printf("\n");

		printf("Exports the entities in the BSP to a text file: `file.ent`\n");
		return;
	}

	if (Q_stricmp(arg, "analyze") == 0 || Q_stricmp(arg, "analyse") == 0) {
		printf("Usage:\n");
		printf("  ajmap3 -analyze <file.bsp> [-lumpswap]\n");
		printf("\n");

		printf("Analyzes all the lumps inside a BSP file, showing detailed information\n");
		printf("about each one and uses heuristics to determine the possible contents.\n");
		printf("\n");
		printf("The -lumpswap option causes the lump directory to be read in a different\n");
		printf("format, which (I think) allows the command to work on Call of Duty maps.\n");
		printf("It has nothing to do with endianness.\n");
		return;
	}

	if (Q_stricmp(arg, "info") == 0) {
		printf("Usage:\n");
		printf("  ajmap3 -info <file.bsp> ...\n");
		printf("\n");

		printf("Displays statistics about the contents of a BSP file.\n");
		return;
	}

	if (Q_stricmp(arg, "fixaas") == 0) {
		printf("Usage:\n");
		printf("  ajmap3 -fixaas <file.bsp>\n");
		printf("\n");

		printf("Fixes the checksum in the corresponding .aas file (used for bot\n");
		printf("navigation) to match the given .bsp file, forcing the game engine\n");
		printf("to accept it.  This is strongly discouraged, it is much safer to\n");
		printf("simply rebuild the .aas file using BSPC or AJBOT3.\n");
		return;
	}

	Sys_FPrintf( SYS_ERR, "Unknown help topic: '%s'\n", arg );
	Sys_FPrintf( SYS_BIG, "(common topics are: commands, games, bsp, light, vis)\n" );
}
