/*
   Copyright (C) 1999-2006 Id Software, Inc. and contributors.
   For a list of contributors, see the accompanying CONTRIBUTORS file.

   This file is part of GtkRadiant.

   GtkRadiant is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   GtkRadiant is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GtkRadiant; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __INOUT__
#define __INOUT__

// andrewj: removed xml stuff

void Broadcast_Setup( const char *dest );
void Broadcast_Shutdown();

#define SYS_STD  0 // standard print level
#define SYS_BIG  1 // important messages
#define SYS_WRN  2 // warnings
#define SYS_ERR  3 // error

#define SYS_VRBflag  0x80 // extra detail (additive flag)

#define SYS_VRB  (SYS_STD | SYS_VRBflag)

extern qboolean verbose;
extern qboolean extra;

void Sys_LogOpen( const char *filename, qboolean append );
void Sys_LogClose();

const char *Sys_LogNameFromSource( const char *source );
const char *Sys_CfgNameFromSource( const char *source );
const char *Sys_DirNameFromSource( const char *source );

void Sys_Printf( const char *text, ... ) __attribute__ ((format (printf, 1, 2)));
void Sys_FPrintf( int flag, const char *text, ... ) __attribute__ ((format (printf, 2, 3)));
void Sys_Warning( const char *format, ... ) __attribute__ ((format (printf, 1, 2)));

// andrewj: added this API for progress messages.
void Progress_Begin();
void Progress_Update( const char *description, int percent );
void Progress_End();

#endif
