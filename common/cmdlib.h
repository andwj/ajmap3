/*
   Copyright (C) 1999-2006 Id Software, Inc. and contributors.
   For a list of contributors, see the accompanying CONTRIBUTORS file.

   This file is part of GtkRadiant.

   GtkRadiant is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   GtkRadiant is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GtkRadiant; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef __CMDLIB__
#define __CMDLIB__

#include "q_platform.h"
#include "bytebool.h"


#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif

#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif


#define MAX_OSPATH  4096

#define MEM_BLOCKSIZE 4096

void *safe_malloc( size_t size );
void *safe_malloc_info( size_t size, const char *info );

#define Q_strcasecmp   Q_stricmp
#define Q_strncasecmp  Q_stricmpn

int Q_stricmp( const char *s1, const char *s2 );
int Q_stricmpn( const char *s1, const char *s2, int n );

char *Q_strlwr( char *in );
char *Q_strupr( char *in );

char *Q_strncpyz( char *dst, const char *src, size_t len );
char *Q_strcat( char *dst, size_t dlen, const char *src );
char *Q_strncat( char *dst, size_t dlen, const char *src, size_t slen );
char *Q_strdup( const char *s );

int Q_filelength( FILE *f );
void Q_getwd( char *out );
void Q_mkdir( const char *path );

char *ExpandArg( const char *path );    // from cmd line
void ExpandWildcards( int *argc, char ***argv );


double I_FloatTime( void );

// NOTE WELL: format string for Error() should NOT end with \n!!
void    Error( const char *error, ... ) __attribute__((noreturn));

FILE    *SafeOpenWrite( const char *filename );
FILE    *SafeOpenRead( const char *filename );
void    SafeRead( FILE *f, void *buffer, int count );
void    SafeWrite( FILE *f, const void *buffer, int count );

int     LoadFile( const char *filename, void **bufferptr );
int   LoadFileBlock( const char *filename, void **bufferptr );
int     TryLoadFile( const char *filename, void **bufferptr );
void    SaveFile( const char *filename, const void *buffer, int count );
qboolean    FileExists( const char *filename );

void    DefaultExtension( char *path, const char *extension );
void    DefaultPath( char *path, const char *basepath );
void    StripFilename( char *path );
void    StripExtension( char *path );

void    ExtractFilePath( const char *path, char *dest );
void    ExtractFileBase( const char *path, char *dest );
void    ExtractFileExtension( const char *path, char *dest );

int     ParseNum( const char *str );


#define MAX_TOKEN_CHARS  1024    // max length of a token from COM_Parse()

const char *COM_Parse( const char **data );
const char *COM_ParseExt( const char **data, qboolean allowLineBreaks );


void CRC_Init( unsigned short *crcvalue );
void CRC_ProcessByte( unsigned short *crcvalue, byte data );
unsigned short CRC_Value( unsigned short crcvalue );

void    Q_CreatePath( const char *path );
void    Q_CopyFile( const char *from, const char *to );

// for compression routines
typedef struct
{
	void    *data;
	int count, width, height;
} cblock_t;


#endif
