/*
   Copyright (C) 1999-2006 Id Software, Inc. and contributors.
   For a list of contributors, see the accompanying CONTRIBUTORS file.

   This file is part of GtkRadiant.

   GtkRadiant is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   GtkRadiant is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GtkRadiant; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

//-----------------------------------------------------------------------------
//
// DESCRIPTION:
//   deal with in/out tasks, especially stdout/stderr printing.
//
//-----------------------------------------------------------------------------

#include <sys/types.h>
#include <sys/stat.h>

#ifdef _WIN32
#include <windows.h>
#include <direct.h>
#endif

#include "cmdlib.h"
#include "mathlib.h"
#include "polylib.h"
#include "qthreads.h"
#include "inout.h"
#include "dye.h"

// andrewj: removed xml stuff

// andrewj: added logging stuff


extern qboolean verbose;
extern qboolean extra;

#define MAX_REMEMBER  64000

typedef struct logfile_s
{
	char filename[MAX_OSPATH];
	FILE *fp;

	// remember messages before log file is created
	char remember[MAX_REMEMBER];
} logfile_t;

static logfile_t logfile;

static void Progress_ClearLine();


void Sys_LogOpen( const char *filename, qboolean append ) {
	if (!filename || !strlen(filename))
		return;

	if (logfile.fp) {
		printf("log file %s is already opened\n", logfile.filename);
		return;
	}

	// andrewj: open in TEXT mode, so that Windows gets \r\n line endings
	const char *mode = append ? "a" : "w";
	logfile.fp = fopen(filename, mode);
	if (!logfile.fp) {
		printf("can't open log file %s\n", filename);
		return;
	}

	Q_strncpyz(logfile.filename, filename, (int)sizeof(logfile.filename));

	// write any pending messages
	fprintf(logfile.fp, "%s", logfile.remember);
	fflush(logfile.fp);

	logfile.remember[0] = 0;
}

void Sys_LogClose() {
	if (logfile.fp) {
		fclose(logfile.fp);
		logfile.fp = NULL;
	}
}

const char *Sys_LogNameFromSource( const char *source ) {
	static char logname[MAX_OSPATH];

	Q_strncpyz(logname, source, sizeof(logname));
	StripExtension(logname);
	Q_strcat(logname, sizeof(logname), ".log");

	return logname;
}

const char *Sys_CfgNameFromSource( const char *source ) {
	static char cfgname[MAX_OSPATH];

	Q_strncpyz(cfgname, source, sizeof(cfgname));
	StripExtension(cfgname);
	Q_strcat(cfgname, sizeof(cfgname), ".cfg");

	return cfgname;
}

const char *Sys_DirNameFromSource( const char *source ) {
	static char dirname[MAX_OSPATH];

	Q_strncpyz(dirname, source, sizeof(dirname));
	StripFilename(dirname);

	if (strlen(dirname) == 0) {
		return ".";
	}

	return dirname;
}

void Broadcast_Setup( const char *dest ) {
}

void Broadcast_Shutdown() {
}

// all output ends up through here
static void RawFPrintf( int flag, const char *buf ) {
	ThreadLock();

	if (verbose || (flag & 7) >= SYS_BIG) {
		qboolean didcolor = qfalse;

		Progress_ClearLine();

		// andrewj: colorize warning and error messages
		if ((flag & 7) == SYS_WRN) {
			dyefg(stdout, DYE_YELLOW);
			didcolor = qtrue;
		} else if ((flag & 7) == SYS_ERR) {
			dyefg(stdout, DYE_RED);
			didcolor = qtrue;
		}

		printf( "%s", buf );

		if (didcolor)
			dyefg(stdout, DYE_RESET);

		fflush(stdout);
	}

	if (logfile.fp) {
		fprintf(logfile.fp, "%s", buf);
		fflush(logfile.fp);
	} else {
		if (strlen(logfile.remember) + strlen(buf) + 4 < MAX_REMEMBER) {
			strcat(logfile.remember, buf);
		}
	}

	ThreadUnlock();
}

void Sys_FPrintf( int flag, const char *format, ... ) {
	char out_buffer[4096];
	va_list argptr;

	if ( ( flag & SYS_VRBflag ) && !extra ) {
		return;
	}

	va_start( argptr, format );
	vsnprintf( out_buffer, sizeof(out_buffer), format, argptr );
	va_end( argptr );

	RawFPrintf( flag, out_buffer );
}

void Sys_Printf( const char *format, ... ) {
	char out_buffer[4096];
	va_list argptr;

	va_start( argptr, format );
	vsnprintf( out_buffer, sizeof(out_buffer), format, argptr );
	va_end( argptr );

	RawFPrintf( SYS_STD, out_buffer );
}

void Sys_Warning( const char *format, ... ) {
	char out_buffer[4096];
	va_list argptr;

	va_start( argptr, format );
	strcpy( out_buffer, "WARNING: " );
	vsnprintf( out_buffer + strlen(out_buffer), sizeof(out_buffer) - strlen(out_buffer), format, argptr );
	va_end( argptr );

	RawFPrintf( SYS_WRN, out_buffer );
}

/*
=================
Error

For abnormal program terminations
=================
*/
void Error( const char *error, ... ) {
	char out_buffer[4096];
	char tmp[4096];
	va_list argptr;

	va_start( argptr, error );
	vsnprintf( tmp, sizeof(tmp), error, argptr );
	va_end( argptr );

	snprintf( out_buffer, sizeof(out_buffer), "************ ERROR ************\n%s\n", tmp );

	RawFPrintf( SYS_ERR, out_buffer );

	exit( 1 );
}

//----------------------------------------------------------------------

// andrewj: added this API for progress messages.

static qboolean prog_active;
static int prog_last_shown;

static void Progress_ClearLine() {
	if (prog_active) {
		fprintf(stderr, "                                                  \r");
	}
}

void Progress_Begin() {
	prog_active = qtrue;
	prog_last_shown = -1;

	// ensure messages on stdout have been shown
	fflush(stdout);
}

void Progress_End() {
	Progress_ClearLine();

	prog_active = qfalse;
	prog_last_shown = -1;
}

void Progress_Update( const char *description, int percent ) {
	if (percent == prog_last_shown)
		return;

	prog_last_shown = percent;

	fprintf(stderr, "%6d%% %s   \r", percent, description);
}
