/*
   Copyright (C) 1999-2006 Id Software, Inc. and contributors.
   For a list of contributors, see the accompanying CONTRIBUTORS file.

   This file is part of GtkRadiant.

   GtkRadiant is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   GtkRadiant is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GtkRadiant; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

//
// andrewj: made big changes here (April 2019).
//

#include "cmdlib.h"
#include "mathlib.h"
#include "inout.h"
#include "qthreads.h"

#define MAX_THREADS  16

/* ydnar: cranking stack size to eliminate radiosity crash with 1MB stack on win32 */
#define THREAD_STACK_SIZE  (4096 * 1024)

int numthreads = 4;

static void ( *workfunction )( int );

static int worktotal;
static const char *workdesc;
static int workdone;

static qboolean showprogress;

/*
   =============
   GetThreadWork

   =============
 */
static int GetThreadWork( void ) {
	int r;
	int f;

	ThreadLock();

	if ( workdone == worktotal ) {
		ThreadUnlock();
		return -1;
	}

	f = 100 * workdone / worktotal;

	Progress_Update( workdesc, f );

	r = workdone;
	workdone++;

	ThreadUnlock();

	return r;
}

static void * ThreadWorkerFunction( void *arg ) {
	while ( 1 )
	{
		int work = GetThreadWork();
		if ( work == -1 ) {
			break;
		}

		workfunction( work );
	}

	return NULL;
}


/*
   ===================================================================

   NO THREADING

   ===================================================================
 */
#if defined(NO_THREADS)

void ThreadInit( void ) {
	numthreads = 1;

	Sys_FPrintf( SYS_BIG, "Using 1 thread (multiple threads not supported)\n" );
}

void ThreadLock( void ) {
}

void ThreadUnlock( void ) {
}

/*
   =============
   RunThreadsOn
   =============
 */
void RunThreadsOn( const char *description, int workcnt, qboolean progress, void ( *func )( int ) ) {
	workfunction = func;
	worktotal = workcnt;
	workdesc = description;
	workdone = 0;

	showprogress = progress;

	if ( showprogress ) {
		Progress_Begin();
	}

	ThreadWorkerFunction(NULL);

	if ( showprogress ) {
		Progress_End();
	}
}

/*
   ===================================================================

   WINDOWS

   ===================================================================
 */

#elif defined(_WIN32)

#include <windows.h>

static CRITICAL_SECTION global_lock;
static int in_critical;

void ThreadInit( void ) {
	if ( numthreads < 1 )
		numthreads = 1;
	else if ( numthreads > MAX_THREADS )
		numthreads = MAX_THREADS;

	Sys_FPrintf( SYS_BIG, "Using %i thread(s)\n", numthreads );
}

void ThreadLock( void ) {
	if ( numthreads > 1 ) {
		EnterCriticalSection( &global_lock );

		if ( in_critical ) {
			Error( "Recursive ThreadLock" );
		}
		in_critical = 1;
	}
}

void ThreadUnlock( void ) {
	if ( numthreads > 1 ) {

		if ( !in_critical ) {
			Error( "ThreadUnlock without lock" );
		}
		in_critical = 0;

		LeaveCriticalSection( &global_lock );
	}
}

/*
   =============
   RunThreadsOn
   =============
 */
void RunThreadsOn( const char *description, int workcnt, qboolean progress, void ( *func )( int ) ) {
	DWORD  threadid[MAX_THREADS];
	HANDLE threadhandle[MAX_THREADS];
	int i;

	workfunction = func;
	worktotal = workcnt;
	workdesc = description;
	workdone = 0;

	showprogress = progress;

	if ( showprogress ) {
		Progress_Begin();
	}

	InitializeCriticalSection( &global_lock );

	if ( numthreads == 1 ) {
		// use same thread as main()
		ThreadWorkerFunction(NULL);

	} else {
		// run threads in parallel
		for ( i = 0 ; i < numthreads ; i++ ) {
			threadhandle[i] = CreateThread(
				NULL,   // LPSECURITY_ATTRIBUTES lpsa,
				THREAD_STACK_SIZE, // DWORD cbStack,
				(LPTHREAD_START_ROUTINE)ThreadWorkerFunction, // LPTHREAD_START_ROUTINE lpStartAddr,
				(LPVOID)NULL,  // LPVOID lpvThreadParm,
				0,             // DWORD fdwCreate,
				&threadid[i] );
		}

		for ( i = 0 ; i < numthreads ; i++ ) {
			WaitForSingleObject( threadhandle[i], INFINITE );
		}
	}

	DeleteCriticalSection( &global_lock );

	if ( showprogress ) {
		Progress_End();
	}
}


/*
   ===================================================================

   POSIX THREADS

   ===================================================================
 */

#else

#include <pthread.h>

static pthread_mutex_t global_lock;
static int in_critical;

void ThreadInit( void ) {
	if ( numthreads < 1 )
		numthreads = 1;
	else if ( numthreads > MAX_THREADS )
		numthreads = MAX_THREADS;

	Sys_FPrintf( SYS_BIG, "Using %i thread(s)\n", numthreads );

	pthread_mutex_init( &global_lock, NULL );
}

void ThreadLock( void ) {
	if ( numthreads > 1 ) {
		pthread_mutex_lock( &global_lock );

		if ( in_critical ) {
			Error( "Recursive ThreadLock" );
		}
		in_critical = 1;
	}
}

void ThreadUnlock( void ) {
	if ( numthreads > 1 ) {
		if ( !in_critical ) {
			Error( "ThreadUnlock without lock" );
		}
		in_critical = 0;

		pthread_mutex_unlock( &global_lock );
	}
}

/*
   =============
   RunThreadsOn
   =============
 */
void RunThreadsOn( const char *description, int workcnt, qboolean progress, void ( *func )( int ) ) {
	int i;
	pthread_t work_threads[MAX_THREADS];
	pthread_attr_t attrib;

	workfunction = func;
	worktotal = workcnt;
	workdesc = description;
	workdone = 0;

	showprogress = progress;

	if ( showprogress ) {
		Progress_Begin();

#if 0  // andrewj: this not needed, progress stuff does a fflush()
		setbuf( stdout, NULL );
#endif
	}

	if ( numthreads == 1 ) {
		// use same thread as main()
		ThreadWorkerFunction(NULL);

	} else {
		// run threads in parallel
		if ( pthread_attr_init( &attrib ) != 0 ) {
			Error( "pthread_attr_create failed" );
		}
		if ( pthread_attr_setstacksize( &attrib, THREAD_STACK_SIZE ) == -1 ) {
			Error( "pthread_attr_setstacksize failed" );
		}

		for ( i = 0 ; i < numthreads ; i++ ) {
			if ( pthread_create( &work_threads[i], &attrib, ThreadWorkerFunction, NULL ) != 0 ) {
				Error( "pthread_create failed" );
			}
		}

		for ( i = 0 ; i < numthreads ; i++ ) {
			if ( pthread_join( work_threads[i], NULL ) != 0 ) {
				Error( "pthread_join failed" );
			}
		}
	}

	if ( showprogress ) {
		Progress_End();
	}
}

#endif
