/*
   Copyright (c) 2001, Loki software, inc.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification,
   are permitted provided that the following conditions are met:

   Redistributions of source code must retain the above copyright notice, this list
   of conditions and the following disclaimer.

   Redistributions in binary form must reproduce the above copyright notice, this
   list of conditions and the following disclaimer in the documentation and/or
   other materials provided with the distribution.

   Neither the name of Loki software nor the names of its contributors may be used
   to endorse or promote products derived from this software without specific prior
   written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS''
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
   DIRECT,INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
   ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
// Rules:
//
// - Pak files are searched first inside the directories.
// - Case insensitive.
// - Unix-style slashes (/) (windows is backwards .. everyone knows that)
//
// Leonardo Zide (leo@lokigames.com)
//

#include <sys/stat.h>

#include "cmdlib.h"
#include "filematch.h"
#include "mathlib.h"
#include "inout.h"
#include "vfs.h"
#include "unzip.h"

typedef struct VFS_PAKFILE_s
{
	char *unzFilePath;
	char *name;
	unz_s zipinfo;
	unzFile zipfile;
	unsigned long size;
	struct VFS_PAKFILE_s *next;
} VFS_PAKFILE;

// =============================================================================
// Global variables

#define MAX_UNZFILES  256
static unzFile g_unzFiles[MAX_UNZFILES];
static int g_unzFilesCount;

static VFS_PAKFILE *g_pakFiles;

static char g_strDirs[VFS_MAXDIRS][MAX_OSPATH + 1];
static int g_numDirs;

static char g_strLoadedFileLocation[MAX_OSPATH];

// =============================================================================
// Utility functions

static void vfsAddSlash( char *str ) {
	int n = strlen( str );
	if ( n > 0 ) {
		if ( str[n - 1] != '\\' && str[n - 1] != '/' ) {
			strcat( str, "/" );
		}
	}
}

static void vfsFixDOSName( char *src ) {
	if ( src == NULL ) {
		return;
	}

	while ( *src )
	{
		if ( *src == '\\' ) {
			*src = '/';
		}
		src++;
	}
}

// =============================================================================
// Directory reading functions
// written by andrewj.

#ifdef _WIN32

typedef struct
{
	HANDLE hnd;
	WIN32_FIND_DATA fdata;
	qboolean finished;
} AJDirInfo;

static AJDirInfo *aj_dir_open(const char *path) {
	static AJDirInfo dir;
	static char pattern[MAX_OSPATH];

	strncpy(pattern, path, sizeof(pattern));
	pattern[sizeof(pattern)-5] = 0;  // room for slash and "*"

	vfsAddSlash(pattern);
	strcat(pattern, "*");

	dir.hnd = FindFirstFile(pattern, &dir.fdata);
	dir.finished = qfalse;

	if (dir.hnd == INVALID_HANDLE_VALUE)
		return NULL;

	return &dir;
}

static void aj_dir_close(AJDirInfo *dir) {
	if (dir != NULL) {
		FindClose(dir->hnd);
	}
}

static const char *aj_dir_read_name(AJDirInfo *dir) {
	static char name_buf[MAX_OSPATH];

	if (dir == NULL || dir->finished)
		return NULL;

	strncpy(name_buf, dir->fdata.cFileName, sizeof(name_buf));
	name_buf[sizeof(name_buf)-1] = 0;

	// get next entry
	if (FindNextFile(dir->hnd, &dir->fdata) != 0)
		dir->finished = qtrue;

	return name_buf;
}

#else // UNIX and MACOSX

#include <dirent.h>

#define AJDirInfo  DIR

static AJDirInfo *aj_dir_open(const char *path) {
	return opendir(path);
}

static void aj_dir_close(AJDirInfo *dir) {
	closedir(dir);
}

static const char *aj_dir_read_name(AJDirInfo *dir) {
	static char name_buf[MAX_OSPATH];

	const struct dirent *fdata = readdir(dir);
	if (fdata == NULL)
		return NULL;

	strncpy(name_buf, fdata->d_name, sizeof(name_buf));
	name_buf[sizeof(name_buf)-1] = 0;

	return name_buf;
}

#endif

// =============================================================================

static void vfsAppendPakFile(VFS_PAKFILE *file) {
	file->next = NULL;

	if (g_pakFiles == NULL) {
		g_pakFiles = file;
		return;
	}

	VFS_PAKFILE *tmp = g_pakFiles;

	while (tmp && tmp->next)
		tmp = tmp->next;

	tmp->next = file;
}

//!\todo Define globally or use heap-allocated string.
#define NAME_MAX 255

static void vfsInitPakFile( const char *filename ) {
	unz_global_info gi;
	unzFile uf;
	unsigned long i;
	int err;

	uf = unzOpen( filename );
	if ( uf == NULL ) {
		return;
	}

	if (g_unzFilesCount >= MAX_UNZFILES) {
		Error("Too many pak files (limit is %d)", MAX_UNZFILES);
		return;
	}

	Sys_Printf( "   %s\n", filename );

	g_unzFiles[g_unzFilesCount++] = uf;

	err = unzGetGlobalInfo( uf,&gi );
	if ( err != UNZ_OK ) {
		return;
	}
	unzGoToFirstFile( uf );

	char *unzFilePath = strdup( filename );

	for ( i = 0; i < gi.number_entry; i++ )
	{
		char filename_inzip[NAME_MAX];
		unz_file_info file_info;
		VFS_PAKFILE* file;

		err = unzGetCurrentFileInfo( uf, &file_info, filename_inzip, sizeof( filename_inzip ), NULL, 0, NULL, 0 );
		if ( err != UNZ_OK ) {
			break;
		}

		file = (VFS_PAKFILE*)safe_malloc( sizeof( VFS_PAKFILE ) );

		vfsFixDOSName( filename_inzip );
		Q_strlwr( filename_inzip );

		file->name = strdup( filename_inzip );
		file->size = file_info.uncompressed_size;
		file->zipfile = uf;
		file->unzFilePath = unzFilePath;
		memcpy( &file->zipinfo, uf, sizeof( unz_s ) );

		vfsAppendPakFile(file);

		if ( ( i + 1 ) < gi.number_entry ) {
			err = unzGoToNextFile( uf );
			if ( err != UNZ_OK ) {
				break;
			}
		}
	}
}

// =============================================================================
// Global functions

// reads all pak files from a dir
void vfsInitDirectory( const char *path, qboolean find_paks ) {
	char filename[MAX_OSPATH];
	char *dirlist;
	AJDirInfo *dir;

	if ( g_numDirs == VFS_MAXDIRS ) {
		Error( "VFS_Init: too many paths!" );
	}

	Sys_Printf( "VFS Path: %s\n", path );

	strncpy( g_strDirs[g_numDirs], path, MAX_OSPATH );
	g_strDirs[g_numDirs][MAX_OSPATH] = 0;
	vfsFixDOSName( g_strDirs[g_numDirs] );
	vfsAddSlash( g_strDirs[g_numDirs] );
	g_numDirs++;

	if (find_paks) {
		dir = aj_dir_open( path );

		if ( dir != NULL ) {
			while ( 1 ) {
				const char* name = aj_dir_read_name( dir );
				if ( name == NULL ) {
					break;
				}

				dirlist = strdup( name );

				{
					char *ext = strrchr( dirlist, '.' );

					if ( ext && !Q_stricmp( ext, ".pk3dir" ) ) {
						if ( g_numDirs == VFS_MAXDIRS ) {
							continue;
						}
						snprintf( g_strDirs[g_numDirs], MAX_OSPATH, "%s/%s", path, name );
						g_strDirs[g_numDirs][MAX_OSPATH] = '\0';
						vfsFixDOSName( g_strDirs[g_numDirs] );
						vfsAddSlash( g_strDirs[g_numDirs] );
						++g_numDirs;
					}

					if ( ( ext == NULL ) || ( Q_stricmp( ext, ".pk3" ) != 0 ) ) {
						continue;
					}
				}

				snprintf( filename, sizeof(filename), "%s/%s", path, dirlist );
				vfsInitPakFile( filename );

				free( dirlist );
			}
			aj_dir_close( dir );
		}
	}
}

// frees all memory that we allocated
void vfsShutdown(void) {
	int i;

	for (i = 0 ; i < g_unzFilesCount ; i++) {
		unzClose( g_unzFiles[i] );
	}

	while (g_pakFiles) {
		VFS_PAKFILE *file = g_pakFiles;
		g_pakFiles = file->next;

		free( file->unzFilePath );
		free( file->name );
		free( file );
	}
}

// return the number of files that match
int vfsGetFileCount( const char *filename ) {
	int i, count = 0;
	char fixed[NAME_MAX], tmp[NAME_MAX];
	VFS_PAKFILE *file;

	strcpy( fixed, filename );
	vfsFixDOSName( fixed );
	Q_strlwr( fixed );

	for (file = g_pakFiles ; file != NULL ; file = file->next) {
		if (strcmp(file->name, fixed) == 0) {
			count++;
		}
	}

	for ( i = 0; i < g_numDirs; i++ )
	{
		strcpy( tmp, g_strDirs[i] );
		strcat( tmp, fixed );
		if ( access( tmp, R_OK ) == 0 ) {
			count++;
		}
	}

	return count;
}

// NOTE: when loading a file, you have to allocate one extra byte and set it to \0
int vfsLoadFile( const char *filename, void **bufferptr, int index ) {
	int i, count = 0;
	char tmp[NAME_MAX], fixed[NAME_MAX];
	VFS_PAKFILE *file;

	// filename is a full path
	if ( index == -1 ) {
		Q_strncpyz( g_strLoadedFileLocation, filename, sizeof(g_strLoadedFileLocation) );
		long len;
		FILE *f;

		f = fopen( filename, "rb" );
		if ( f == NULL ) {
			return -1;
		}

		fseek( f, 0, SEEK_END );
		len = ftell( f );
		rewind( f );

		*bufferptr = safe_malloc( len + 1 );
		if ( *bufferptr == NULL ) {
			fclose( f );
			return -1;
		}

		if ( fread( *bufferptr, 1, len, f ) != (size_t) len ) {
			fclose( f );
			return -1;
		}
		fclose( f );

		// we need to end the buffer with a 0
		( (char*) ( *bufferptr ) )[len] = 0;

		return len;
	}

	*bufferptr = NULL;
	strcpy( fixed, filename );
	vfsFixDOSName( fixed );
	Q_strlwr( fixed );

	// andrewj: go backwards, give higher priority to last added directory
	for ( i = g_numDirs-1; i >= 0; i-- )
	{
		strcpy( tmp, g_strDirs[i] );
		strcat( tmp, filename );
		if ( access( tmp, R_OK ) == 0 ) {
			if ( count == index ) {
				Q_strncpyz( g_strLoadedFileLocation, tmp, sizeof(g_strLoadedFileLocation) );

				long len;
				FILE *f;

				f = fopen( tmp, "rb" );
				if ( f == NULL ) {
					return -1;
				}

				fseek( f, 0, SEEK_END );
				len = ftell( f );
				rewind( f );

				*bufferptr = safe_malloc( len + 1 );
				if ( *bufferptr == NULL ) {
					fclose( f );
					return -1;
				}

				if ( fread( *bufferptr, 1, len, f ) != (size_t) len ) {
					fclose( f );
					return -1;
				}
				fclose( f );

				// we need to end the buffer with a 0
				( (char*) ( *bufferptr ) )[len] = 0;

				return len;
			}

			count++;
		}
	}

	for (file = g_pakFiles ; file != NULL ; file = file->next) {
		if (strcmp(file->name, fixed) != 0) {
			continue;
		}

		if ( count == index ) {
			Q_strncpyz( g_strLoadedFileLocation, file->unzFilePath, sizeof(g_strLoadedFileLocation) );
			Q_strcat( g_strLoadedFileLocation, sizeof(g_strLoadedFileLocation), " :: " );
			Q_strcat( g_strLoadedFileLocation, sizeof(g_strLoadedFileLocation), filename );

			memcpy( file->zipfile, &file->zipinfo, sizeof( unz_s ) );

			if ( unzOpenCurrentFile( file->zipfile ) != UNZ_OK ) {
				return -1;
			}

			*bufferptr = safe_malloc( file->size + 1 );
			// we need to end the buffer with a 0
			( (char*) ( *bufferptr ) )[file->size] = 0;

			i = unzReadCurrentFile( file->zipfile, *bufferptr, file->size );
			unzCloseCurrentFile( file->zipfile );
			if ( i < 0 ) {
				return -1;
			} else {
				return file->size;
			}
		}

		count++;
	}

	return -1;
}

const char *vfsFileLocation(void) {
	return g_strLoadedFileLocation;
}
